
<h3><?php echo $data['email']; ?> has deposited and is requesting wallet credit.</h3>
<p><b>Deposit Details</b></p>
<p><?php echo "Bank: " . $data['bank_name']; ?>
</p>
<p>Account No.: <?php echo $data['account_number']; ?></p>
<p>Account Name.: <?php echo $data['account_name']; ?></p>
<p>Amount: <?php echo $data['currency'] . ' ' . number_format($data['amount'], 2); ?></p>
<br>
<p>Check request in <b><i><a href="http://admin.tagbond.com">http://admin.tagbond.com</a></i></b></p>