<tr style="background-color: #eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i><b style='text-transform:uppercase'>Reset Pin Code</b></i></h1>
	</td>
</tr>
<tr>
	<td>		
		<p>
			You have requested for a new Pin Code.
			Your new Pin Code is: <?php echo $newPin; ?>
			<br/>
			<small><?php echo $user_email;?></small>
		</p>
		<p>
			<?php echo Yii::t("labels", "Thank you!"); ?><br><br>
			<b><?php echo Yii::t("labels", "Tagcash"); ?></b>
		</p>
	</td>
</tr>

