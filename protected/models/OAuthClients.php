<?php

/**
 * This is the model class for table "oauth_clients".
 *
 * The followings are the available columns in table 'oauth_clients':
 * @property string $id
 * @property string $secret
 * @property string $name
 * @property integer $auto_approve
 *
 * The followings are the available model relations:
 * @property CommunityOauthclients[] $communityOauthclients
 * @property OauthClientEndpoints[] $oauthClientEndpoints
 * @property OauthSessionRefreshTokens[] $oauthSessionRefreshTokens
 * @property OauthSessions[] $oauthSessions
 */
class OAuthClients extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OAuthClients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oauth_clients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, secret, name', 'required'),
			array('auto_approve', 'numerical', 'integerOnly'=>true),
			array('id, secret', 'length', 'max'=>40),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, secret, name, auto_approve', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'details'=>array(self::HAS_ONE, 'OauthclientDetails', 'client_id'),
			'communityOauthclients' => array(self::HAS_MANY, 'CommunityOauthclients', 'client_id'),
			'endpoint' => array(self::HAS_ONE, 'OauthClientEndpoints', 'client_id'),
			'oauthSessionRefreshTokens' => array(self::HAS_MANY, 'OauthSessionRefreshTokens', 'client_id'),
			'oauthSessions' => array(self::HAS_MANY, 'OauthSessions', 'client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'secret' => 'Secret',
			'name' => 'Name',
			'auto_approve' => 'Auto Approve',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('secret',$this->secret,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('auto_approve',$this->auto_approve);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}



	public static function getClientDetails($id){
		$result = Yii::app()->db->createCommand()
				->select('t1.name, t2.*, t3.*')
				->from('oauth_clients t1')
				->leftJoin("oauthclient_details t2", "t1.id = t2.client_id")
				->leftJoin("communities t3", "t3.id = t2.community_id")
				->where("t1.id = :clientId", array("clientId" => $id))
				->queryRow();

		if($result){
			return $result;
		}
		else{
			return false;
		}
	}


	public static function getAllowedGrantTypes($id){
		$result = Yii::app()->db->createCommand()
				->select('t1.community_id, t1.oauth_granttypes')
				->from('oauthclient_details t1')
				->where("t1.client_id = :clientId", array("clientId" => $id))
				->queryRow();

		if($result){
			if(!$result['community_id']){
				//Means if it is a system app
				$grantTypes = array(
					'authorization_code',
					'implicit',
					'password',
					'refresh_token'
				);
			}
			else{
				$grantTypes = explode(",",$result['oauth_granttypes']);	
			}

			return $grantTypes;
		}
		else{
			return false;
		}
	}


	public static function checkIfAllowedGrant($id, $type){
		$grantTypes = self::getAllowedGrantTypes($id);
		if($grantTypes){
			if(in_array($type, $grantTypes)){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}



	public static function approvedBefore($clientId, $id, $type, $requestedScopes){
		$result = Yii::app()->db->createCommand()
				->select('t4.scope')
				->from('oauth_sessions t1')
				->join('oauth_session_access_tokens t2', 't1.id = t2.session_id')
				->join('oauth_session_token_scopes t3', 't2.id = t3.session_access_token_id')
				->join('oauth_scopes t4', 't4.id = t3.scope_id')

				->where("t1.client_id = :clientId AND t1.owner_type = :type AND t1.owner_id = :id", array("clientId" => $clientId, 
					"type" => $type, "id" => $id))
				->queryAll();

		$currentScopes = array();

		if($result){
			foreach($result as $s){
				$currentScopes[] = $s['scope'];
			}
		}

		if($requestedScopes){
			foreach($requestedScopes as $requestedScope){
				if(!in_array($requestedScope['scope'], $currentScopes)){
					//If the requested scope is not there in the already existing scopes
					return false;
				}
			}
		}

		return true;
	}

	public function generateCredentials($id = NULL){
		$this->id = hash('adler32',$this->name).hash('crc32b',microtime());
		$this->secret = md5($id.microtime());
		return true;
	}
}