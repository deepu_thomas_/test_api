<tr style="background-color:#eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top: 50px;"><i>Deal Invitation</i></h1>
	</td>
</tr>
<tr>
	<td>
		<p><?php echo Yii::t("labels", "Hi!"); ?></p>

		<p><?php echo $sender->user_firstname." ".$sender->user_lastname.Yii::t("labels", " wants to share the deal ")."'".$deal['title']."'".Yii::t("labels"," with you. Click the link below."); ?></p>

		<p>
			<a href="<?php echo Yii::app()->params['site_url']."/d/".$deal['_id']->__toString().'?u='.$sender->id; ?>">
				<?php echo $deal['title']; ?>
			</a>
		</p>

		<p><?php echo Yii::t("labels", "If you didn't request this email or have no idea why you received it, please ignore it."); ?></p>

		<p>
			<?php echo Yii::t("labels", "Thanks,"); ?><br /><br />
			<b><?php echo Yii::t("labels", "Tagbond") ?></b>
		</p>
	</td>
</tr>