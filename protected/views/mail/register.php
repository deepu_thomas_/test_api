<tr style="background-color: #eb3f3c;">

	<?php if(isset($custom_message['custom_email_title'])) {?>
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i><?php echo $custom_message['custom_email_title']; ?></i></h1>
	</td>
	<?php } 
	else { ?>
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i>Welcome to TAG System!</i></h1>
	</td>
	<?php } ?>
</tr>
<tr>
	<td>
		<p>
			<?php echo Yii::t("labels", "Hi " . $name . "!"); ?>
		</p>
		
		<?php if(isset($custom_message['custom_email_message'])){
			echo "<p>";
			echo CHtml::decode($custom_message['custom_email_message']); 
			echo "</p>";
		}
		else{
			echo Yii::t("labels", "Thank you for registering with tagcash.");
		}

		if($password){ ?>
			<p>
			<?php  echo Yii::t("labels", "Your password is: "); ?>	
			<font color="red"><b><i><?php echo $password; ?></i></b></font>
		<?php }
		if($pin) { ?>
			<p>
			<?php  echo Yii::t("labels", "Your PIN is: "); ?>	
			<font color="red"><b><i><?php echo $pin; ?></i></b></font>
		<?php } ?>

		<?php if(isset($custom_message['custom_link'])){ ?>
            <p>
                Please click <a href="<?php echo $custom_message['custom_link']; ?>">here</a> to proceed <?php echo $custom_message['custom_link_message']; ?>.
            </p>
        <?php } 
        else {?>
		<?php if($normal_register == null){?>
			<p>
			<?php echo Yii::t("labels", "Please click on the confirmation link to activate or change password and PIN, add a picture, connect an RFID, get your own personal QR code and much more."); ?></p>

			<p><a href="<?php echo $activationkey; ?>"><?php echo $activationkey; ?></a></p>
			<?php } ?>
		<?php } ?>
		<p><?php echo Yii::t("labels", "The TAG ID system is used within multiple apps and websites and makes it easier to login, manage, share and control your personal information in one place."); ?></p>
		<p><?php echo Yii::t("labels", "If you didn't request this email or have no idea why you received it, please ignore it."); ?></p>

		<p>
			<?php echo Yii::t("labels", "Thank you!"); ?><br />
			<b><?php echo ($custom_message['custom_email_sender'])?$custom_message['custom_email_sender']:"TAG"; ?></b>
		</p>
	</td>
</tr>