<?php

/**
 * This is the model class for table "user_preferences".
 *
 * The followings are the available columns in table 'user_preferences':
 * @property integer $user_id
 * @property integer $pref_pin_login
 * @property integer $pref_wallet_id
 * @property integer $pref_currency
 * @property string $pref_wallet_cash_in
 * @property string $pref_wallet_cash_out
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class UserPreferences extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_preferences';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id, pref_pin_login, pref_wallet_id, pref_currency,pref_isdcode', 'numerical', 'integerOnly'=>true),
			array('pref_wallet_cash_in, pref_wallet_cash_out', 'length', 'max'=>19),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, pref_pin_login, pref_wallet_id, pref_currency, pref_wallet_cash_in, pref_wallet_cash_out,pref_isdcode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'pref_pin_login' => 'Pref Pin Login',
			'pref_wallet_id' => 'Pref Wallet',
			'pref_currency' => 'Pref Currency',
			'pref_wallet_cash_in' => 'Pref Wallet Cash In',
			'pref_wallet_cash_out' => 'Pref Wallet Cash Out',
			'pref_isdcode' => 'Pref ISD Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('pref_pin_login',$this->pref_pin_login);
		$criteria->compare('pref_wallet_id',$this->pref_wallet_id);
		$criteria->compare('pref_currency',$this->pref_currency);
		$criteria->compare('pref_wallet_cash_in',$this->pref_wallet_cash_in,true);
		$criteria->compare('pref_wallet_cash_out',$this->pref_wallet_cash_out,true);
		$criteria->compare('pref_isdcode',$this->pref_isdcode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserPreferences the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function showDefaults(){
		$user = $this->user;

		if($user->user_defaultcommunity_id){
			$community = Communities::model()->findByPk($user->user_defaultcommunity_id);
		}

		if($this->pref_wallet_id){
			$wallet = WalletTypes::model()->findByPk($this->pref_wallet_id);
		}

		$defaults = array();
		$defaults['community_id'] = ($user->user_defaultcommunity_id)?$user->user_defaultcommunity_id:null;
		$defaults['community'] = ($community)?array('name'=>$community->community_name):null;
		$defaults['pin_login'] = ($this->pref_pin_login)?true:false;
		$defaults['currency'] = ($this->pref_currency)?'wallet':'reward';
		$defaults['wallet_id'] = ($this->pref_wallet_id)?$this->pref_wallet_id:null;
		$defaults['wallet_cash_in'] = $this->pref_wallet_cash_in;
		$defaults['wallet_cash_out'] = $this->pref_wallet_cash_out;
		$defaults['wallet'] = ($wallet)?array('name'=>$wallet->wallet_name, 'currency_code'=>$wallet->currency_code, 'wallet_type'=>WalletBalances::formatWalletType($wallet->wallet_type)):null;
		$defaults['isdcode'] = $this->pref_isdcode;
		$defaults['remember_previous_login'] = (bool)$this->save_prev_login;
		
		//show contacts
		$userSettingsObj = UserSettings::model()->findByAttributes(array('user_id'=>$this->user_id));
		$defaults['show_contact_details'] = isset($userSettingsObj)?(bool)$userSettingsObj->user_contact_access_status:(bool)0;
		return $defaults;
	}
}
