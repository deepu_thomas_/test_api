<?php

class PublicController extends Controller {

    public function actionIndex() {
        throw new CHttpException(403, "invalid_request");
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function beforeAction($action){
        set_time_limit (0);
        return parent::beforeAction($action);
    }
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            //We are going to use a hackish method to display the validation errors.Sorry :)
            if (Common::isJson($error['message'])) {
                $this->response['validation_errors'] = json_decode($error['message']);
                $this->response['status'] = "failed";
                $this->response['error'] = "validation_error";
                Rest::sendResponse($error['code'], json_encode($this->response, JSON_NUMERIC_CHECK));
            } else {
                $this->response['status'] = "failed";
                $this->response['error'] = $error['message'];
                Rest::sendResponse($error['code'], json_encode($this->response, JSON_NUMERIC_CHECK));
            }
        }
    }

    public function actionErrorsList() {
        $this->response['result'] = TagErrors::listErrors();
        $this->response['status'] = "success";
        Rest::sendResponse(200, json_encode($this->response, JSON_NUMERIC_CHECK));
    }

   

}
