<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	const ERROR_USER_BANNED = -1;
	private $_id;

	public function authenticate($fb=null, $firstname=null, $lastname=null, $verified=null)
	{
		if(strpos($this->username, '@') !== false){
			$userRecord = Users::model()->findByAttributes(array('user_email'=>$this->username));
		} else {
			$userRecord = Users::model()->findByAttributes(array('id'=>$this->username));
		}

		if($userRecord===null){
			return $this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		# no password for facebook
		else if(!$fb && $userRecord->user_password!=$this->password){
			return $this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		else if($userRecord->user_status!=1){
			return $this->errorCode=self::ERROR_USER_BANNED;
		}

		# set session
		$this->_id=$userRecord->id;
		$this->setState('userId', $userRecord->id);
		$this->setState('userEmail', $userRecord->user_email);
		$this->setState('userFirstname', $userRecord->user_firstname);
		$this->setState('userLastname', $userRecord->user_lastname);
		$this->setState('communityId', '0');

		/* log logins */
		$UserLogins = new UserLogins();
		$UserLogins->log($userRecord->id);
		//echo $userRecord->id; exit;

		// create cookie
		$cookie = new CHttpCookie('uID',$userRecord->id);
		$cookie->domain = '.tagbond.com';
		Yii::app()->request->cookies['uID'] = $cookie;

		return $this->errorCode=self::ERROR_NONE;
	}

	public function getId(){
		return $this->_id;
	}

	public static function setIdentity($thisObj, $currentObj = NULL){
		$result = $currentObj;
		if(!$result){
			$result = new stdClass();
		}

		// community
		if(isset($thisObj->_COMMUNITYID)){
			$result->communityId = $thisObj->_COMMUNITYID;
		}
		else if(isset($thisObj->communityId)){
			$result->communityId = $thisObj->communityId;
		}

		// user
		if(isset($thisObj->_USERID)){
			$result->userId = $thisObj->_USERID;
		}
		else if(isset($thisObj->userId)){
			$result->userId = $thisObj->userId;
		}

		// user obj
		if(isset($thisObj->_USEROBJ)){
			$result->user = $thisObj->_USEROBJ;
		}
		else if(isset($thisObj->user)){
			$result->user = $thisObj->user;
		}

		return $result;
	}
}
