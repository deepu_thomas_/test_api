<?php
include "RestFirebase.php";

class Firebase {
    private $root;
    function __construct($root) {
        $this->root = $root;        
    }
    
    public function child($child) {
        return new Firebase($this->root.'/'.$child);
    }
    
    private function url() {
        return $this->root.'.json';
    }
    
    public function val() {
        $rest = new RestFirebase();
        $snap = $rest->get( $this->url() );
        return json_decode($snap,true);
    }
    
    public function set($data) {
        $rest = new RestFirebase();
        return $rest->post( $this->url(), json_encode($data) );
    }
    
    public function update($data) {
        $rest = new RestFirebase();
        return $rest->patch($this->url(), json_encode($data));
    }
    
    public function push($data) {
        $rest = new RestFirebase();
        return $rest->post($this->url(), json_encode($data));
    }
    
    public function delete() {
        $rest = new RestFirebase();
        $rest->delete($this->url());
    }
}
?>