<?php /*
  preg_match('/.tagcash./', $siteUrl, $matches, PREG_OFFSET_CAPTURE);
  if(count($matches)) {
  $navClass = 'blackbg';
  }
  else {
  $navClass = 'redbg';
  }



  <div class="navbar navbar-fixed-top">
  <div class="navbar-inner <?php echo $navClass ?>">
  <div class="container">
  <a href="#" class="logo clearfix">
  <?php
  if(count($matches)) { ?>
  <img src="<?php echo Yii::app()->createUrl("images/tagcash/logo.png"); ?>" alt="Tagcash Logo"/>
  <?php }
  else { ?>
  <img src="<?php echo Yii::app()->createUrl("images/logo.png"); ?>" alt="Tagbond Logo"/>
  <?php } ?>
  </a>
  </div>
  </div>
  </div>
  <div class="mycontainer clearfix">
  <div class="row-fluid">
  <div class="span12">
  <h4>
  SDK for <?php echo $siteName ?>
  </h4>
  <hr/>
  <ol>
  <li>Create an account in <a href="<?php echo $siteUrl ?>"><?php echo $siteName ?></a>.
  <li>Complete the verification process of <?php echo $siteName ?>.
  <li>Create a community in your account.
  <li>Go to the Developer's page of the community you just created.
  <li>Set the Redirect Uri
  <li>Copy the Client ID, Client Secret and the Redirect Uri </li>
  <li>Use it to setup the SDK for your application </li>
  </ol>
  <hr/>
  <div class="row-fluid">
  <div class="well input-block-level" style="font-size:.9em;">
  Zip File:
  <a href="https://github.com/zeresoft/tagbond_sdk/archive/master.zip"><i class=''></i> <b>https://github.com/zeresoft/tagbond_sdk</b></a>
  <br />
  <br />
  Git Repository:
  <a href="https://github.com/zeresoft/tagbond_sdk" target="_blank"><i class=''></i> <b>https://github.com/zeresoft/tagbond_sdk</b></a>
  <br/>
  <a href="com.tagbondnavigation.showtagvert.callback://www.tagcash.com/d=56b9abc8161f46e85ad05a1e">Deal</a>
  <a href="intent://scan/#Intent;scheme=com.tagbondnavigation.showtagvert.callback;package=com.tagbondnavigation;S.d=56b9abc8161f46e85ad05a1e;i.sharer_id=14918;S.browser_fallback_url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.tagbondnavigation%26d%3D56b9abc8161f46e85ad05a1e%2314918;end"> Take a QR code </a>

  </div>
  </div>
  </div>
  </div>
  </div>
 */
?>
<iframe onload="this.width = screen.width;this.height = 750;" src="http://tagcash.com/swagger_tag/swagger-ui/dist/"></iframe>
<iframe onload="this.width = screen.width;this.height = 800;" src="http://tagcash.com/swagger_tag/community-api/swagger-ui/dist"></iframe>