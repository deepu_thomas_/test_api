<?php

error_reporting(E_ALL ^ E_NOTICE);

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

	public $breadcrumbs = array();
	public $menu = array();
	public $requestParameters;
	public $response = array();
	public $_OAUTH;
	public $_USERID = 0;
	public $_USEROBJ;
	public $_COMMUNITYID;
	public $_COMMUNITYOBJ;
	public $_ADMINID;
	public $_ADMINOBJ;
	public $_ADMINAPP;
	public $_SUPERAPP = false;	//If true, then this app is having all the permissions in the API
	public $_CLIENTLOGIN;	//If true, means this is client_credentials login
	public $_CLIENTDETAILS;
	public $_SETTINGS;

	public function filters(){
		return array(
			'accessControl',
		);
	}

	public function accessRules(){
		$thisObj = $this;

		$header = getallheaders();

		if($header['Authorization']){
			$headerAuth = explode(' ', $header['Authorization']);
			//Common::pre($headerAuth, true);
			if($headerAuth[0] == 'Bearer'){
				//client id and secret
				$credentials = explode(':', League\OAuth2\Server\Resource::base64UrlDecode($headerAuth[1]));
				$_POST['client_id'] = $credentials[0];
				$_POST['client_secret'] = $credentials[1];
			}
		}

		return array(
			array(
				'allow',
				'controllers' => array('site','authentication','oauth','statistics','test','public','tests'),
				'expression' => function(){
					return true;
				}
			),
            //for 7connect redirect url
            array(
                'allow',
                'controllers' => array('credit'),
                'actions' => array('saveCliqqStatus'),
                'expression' => function(){
                    return true;
                }
            ),
			array(
				'allow',
				'controllers' => array('device'),
				'expression'=>function() use ($thisObj){
					try {
						$thisObj->_OAUTH->server->isValid();
						$clientDetails = OAuthClients::getClientDetails($thisObj->_OAUTH->server->getClientId());
						$thisObj->_CLIENTDETAILS = $clientDetails;

						//pre validation
						if(!$clientDetails['login_type'] && $clientDetails['login_id']){
							throw new CHttpException(403, 'permission_denied');
						}

						if($thisObj->_OAUTH->server->getOwnerType() != 'client'){
							if($clientDetails['community_id']){
								throw new CHttpException(403, 'permission_denied');
							}

							$thisObj->_SUPERAPP = true;
							$thisObj->_USERID = $thisObj->_OAUTH->server->getOwnerId();
							$thisObj->_USEROBJ = Users::model()->findByPk($thisObj->_USERID);
							if(!$thisObj->_USEROBJ){
								throw new CHttpException(403, "invalid_login");
							}

							$tokenDetails = OAuthtokenDetails::model()->findByAttributes(array("oauth_token" => $thisObj->_OAUTH->server->getAccessToken()));

							if($tokenDetails->oauth_perspective_id){
								$thisObj->_COMMUNITYID = $tokenDetails->oauth_perspective_id;
								$thisObj->_COMMUNITYOBJ = Communities::model()->findByPk($thisObj->_COMMUNITYID);
							}
						}
					}
					// The access token is missing or invalid...
					catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e){
						//nothing set
					}
				}
			),
			array(
				'allow',
				'controllers' => array('registration'),
				'expression' => function() use ($thisObj){
					if($_POST['client_id'] && $_POST['client_secret']){
						//Check if client exists
						$client = OAuthClients::model()->findByAttributes(array("id" => $_POST['client_id'], "secret" => $_POST['client_secret']));
						if($client){
							//If the client id and client secret exists
							if(!OAuthClients::checkIfAllowedGrant($_POST['client_id'], "authorization_code")){
								//This client doesnt have the permission to use this grant
								throw new CHttpException(403, "grant_not_permitted");
							}
							else{
								return true;
							}
						}

						throw new CHttpException(403, "invalid_client");
					}

					//user registration by merchant token
					try {
						$thisObj->_OAUTH->server->isValid();
						$clientDetails = OAuthClients::getClientDetails($thisObj->_OAUTH->server->getClientId());
						$thisObj->_CLIENTDETAILS = $clientDetails;

						if($thisObj->_OAUTH->server->getOwnerType() == "client"){
							//Means this is a client credentials login
							//So we have to get the community of this client and then switch to that perspective
							$clientDetails = OAuthClients::getClientDetails($thisObj->_OAUTH->server->getOwnerId());
							$thisObj->_CLIENTDETAILS = $clientDetails;
							$thisObj->_CLIENTLOGIN = true;

							$thisObj->_USERID = $clientDetails['user_id'];
							$thisObj->_USEROBJ = Users::model()->findByPk($thisObj->_USERID);
							$thisObj->_COMMUNITYID = $clientDetails['id'];
							$thisObj->_COMMUNITYOBJ = Communities::model()->findByPk($thisObj->_COMMUNITYID);

							if($thisObj->_COMMUNITYOBJ->getVerificationDetails()->merchant_verified){
								return true;
							}

							throw new CHttpException(403, "not_a_merchant");
						}
					}
					// The access token is missing or invalid...
					catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e){
						$thisObj->response['error'] = $e->getMessage();
						$thisObj->response['status'] = "failed";
						$thisObj->response['action'] = "tokenverification";
						Rest::sendResponse(401, CJSON::encode($thisObj->response));
					}
				}
			),
			array(
				'allow',
				'controllers' => array('directWallet'),
				'expression' => function(){
					if($_POST['client_id'] && $_POST['client_secret']){
						//Check if client exists
						$client = OAuthClients::model()->findByAttributes(array("id" => $_POST['client_id'], "secret" => $_POST['client_secret']));
						if($client){
							//If the client id and client secret exists
							if(!OAuthClients::checkIfAllowedGrant($_POST['client_id'], "password")){
								//This client doesnt have the permission to use this grant
								throw new CHttpException(403, "grant_not_permitted");
							}
							else{
								return true;
							}
						}
					}

					throw new CHttpException(403, "invalid_client");
				}
			),
			array(
				'allow',
				'controllers' => array('adminWallet'),
				'expression' => function() use ($thisObj){
					$thisObj->_OAUTH->server->isValid();
					$clientDetails = OAuthClients::getClientDetails($thisObj->_OAUTH->server->getClientId());
					$thisObj->_CLIENTDETAILS = $clientDetails;

					if((!$clientDetails['login_type'] || $clientDetails['login_type'] == 3) && $clientDetails['login_id']){
						$thisObj->_ADMINAPP = true;
						$thisObj->_ADMINID = $thisObj->_OAUTH->server->getOwnerId();
						$thisObj->_ADMINOBJ = Administrators::model()->findByPk($thisObj->_ADMINID);

						if(!$thisObj->_ADMINOBJ){
							throw new CHttpException(403, "invalid_client");
						}

						return true;
					}
				}
			),
			array(
				'deny',
				'expression'=>function() use ($thisObj){
					// Test for token existance and validity
					try {
						$thisObj->_OAUTH->server->isValid();
						$clientDetails = OAuthClients::getClientDetails($thisObj->_OAUTH->server->getClientId());
						$thisObj->_CLIENTDETAILS = $clientDetails;

						//pre validation
						if(!$clientDetails['login_type'] && $clientDetails['login_id']){
							throw new CHttpException(403, "permission_denied");
						}

						if($thisObj->_OAUTH->server->getOwnerType() == "client"){
							//Means this is a client credentials login
							//So we have to get the community of this client and then switch to that perspective
							$clientDetails = OAuthClients::getClientDetails($thisObj->_OAUTH->server->getOwnerId());
							$thisObj->_CLIENTDETAILS = $clientDetails;
							$thisObj->_CLIENTLOGIN = true;

							$thisObj->_USERID = $clientDetails['user_id'];
							$thisObj->_USEROBJ = Users::model()->findByPk($thisObj->_USERID);
							$thisObj->_COMMUNITYID = $clientDetails['id'];
							$thisObj->_COMMUNITYOBJ = Communities::model()->findByPk($thisObj->_COMMUNITYID);
						}
						else{
							if(!$clientDetails['community_id']){
								$thisObj->_SUPERAPP = true;
							}

							$thisObj->_USERID = $thisObj->_OAUTH->server->getOwnerId();
							$thisObj->_USEROBJ = Users::model()->findByPk($thisObj->_USERID);
							if(!$thisObj->_USEROBJ){
								throw new CHttpException(403, "invalid_login");
							}

							$tokenDetails = OAuthtokenDetails::model()->findByAttributes(array("oauth_token" => $thisObj->_OAUTH->server->getAccessToken()));

							if($tokenDetails->oauth_perspective_id){
								$thisObj->_COMMUNITYID = $tokenDetails->oauth_perspective_id;
								$thisObj->_COMMUNITYOBJ = Communities::model()->findByPk($thisObj->_COMMUNITYID);
							}
						}
					}
					// The access token is missing or invalid...
					catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e){
                                                $fb_id=  Common::getFbId();
                                                if(!$fb_id){
						$thisObj->response['error'] = $e->getMessage();
						$thisObj->response['status'] = "failed";
						$thisObj->response['action'] = "tokenverification";
						Rest::sendResponse(401, CJSON::encode($thisObj->response));
                                                }else{
                                                    $res = array(
                                "messages" => array(
                                    array(
                                        "text" => "Login expired - accesstoken verification failed",
                                        "quick_replies" => array(
                                            array(
                                                "title" => "Please Login again",
                                                "block_names" => array("register_login_menu")
                                            )
                                            
                                        )
                                    )
                                )
                            );
                            Rest::sendResponse(403, CJSON::encode($res));
                        }
					}
				}
			)
		);
	}

	// Constructor override
	function __construct($id, $module = null) {
		parent::__construct($id, $module);
		$this->_OAUTH = new stdClass;

		// Initiate the Request handler
		$this->_OAUTH->request = new League\OAuth2\Server\Util\Request();

		// Initiate a new database connection
		//$db = new League\OAuth2\Server\Storage\PDO\Db('mysql://root:@localhost/tagbond_authtest');
		$db = Yii::app()->db;


		// Initiate the auth server with the models
		$this->_OAUTH->server = new League\OAuth2\Server\Resource(
			new League\OAuth2\Server\Storage\PDO\Session($db)
		);

		//setting
		$this->_SETTINGS = SiteSettings::getSettings();
	}
}

