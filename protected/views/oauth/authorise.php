<div id="authorize-page">
	<div class="navbar navbar-fixed-top redbg" style="height:.2em;"></div>

	<div class="mycontainer clearfix">
		<div class="row-fluid">
			<div class="navbar-inner redbg" style="height:3.6em;">
				<div class="span4">
					<a href="#" class="logo clearfix">
						<img src="<?php echo Yii::app()->createUrl("images/logo.png"); ?>" alt="Tagbond Logo"/>
					</a>
				</div>
				<div class="span8">
					<h4>Do you want to share data</h4>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<div class="row-fluid tBreak">
					<div class="span12">
						<div class="row-fluid">
							<h6 style="margin-top:0;">The Community</h6>
						</div>

						<div class="row-fluid">
							<div class="span3">
								<img src="<?php echo Yii::app()->createUrl("images/default-community.png"); ?>" alt="Community Avatar"/>
							</div>
							<div class="span9">
								<a class="titlelink" href="<?php echo Yii::app()->params['site_url'].'/c/'.$clientDetails['id'] ?>"><?php echo $clientDetails['community_name'] ?></a>
								<div class="muted" style="margin-top:5px;">
									<?php
									echo ($clientDetails['community_description'])?$clientDetails['community_description']:"<i>No description available</i>"
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="span6">
				<div class="row-fluid tBreak">
					<div class="span12">
						<div class="row-fluid">
							<h6 style="margin-top:0;">Is requesting permission to</h6>
						</div>

						<div class="row-fluid">

							<?php
							if($userScopes){
								echo "<h4 style='margin-top:0'>User</h4>";
							}
							?>

							<ul>
							<?php
							if($userScopes){
								foreach($userScopes as $scope){
									echo "<li><h6 style='margin-bottom:0'>".$scope['name']."</h6>";
									echo "<p>".$scope['description']."</p></li>";
								}
							}
							else{
								echo "<li><h6 style='margin-bottom:0'>Your basic information</h6>";
								echo "<p>Your First Name, Last Name, Tagbond ID etc</p></li>";
							}
							?>
							</ul>

							<?php
							if($communityScopes){
								echo "<h4 style='margin-top:0'>Community</h4>";
							}
							?>

							<ul>
							<?php
							if($communityScopes){
								foreach($communityScopes as $scope){
									echo "<li><h6 style='margin-bottom:0'>".$scope['name']."</h6>";
									echo "<p>".$scope['description']."</p></li>";
								}
							}
							?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<br/>

		<div class="row-fluid submitfooter">
			<div class="span12">
				<form method="POST">
					<input type="submit" class="submit-btn pull-right whitebg" value="Deny" name="deny"/>
					<input type="submit" class="submit-btn pull-right" value="Accept Request" name="approve"/>
				</form>
			</div>
		</div>
	</div>
</div>
