<tr style="background-color: #eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i>You're Invited!</i></h1>
	</td>
</tr>
<tr>
	<td>
	<?php if(!$communityId){ ?>
	<i>
		<p><?php echo Yii::t("labels", "Hello there!"); ?></p>

		<p><?php echo Yii::t("labels", $name . " would like to exchange contact information with you on Tagcash! Click the link below to register."); ?></p>

		<p>
			<a href="<?php echo Yii::app()->params['site_url']."/".$userId; ?>"><?php echo Yii::t("labels", "Accept Invitation"); ?></a>
		</p>

		<p><?php echo Yii::t("labels", "If you didn't request this email or have no idea why you received it, please ignore it."); ?></p>

		<p>
		<?php echo Yii::t("labels", "Thank you!"); ?><br /><br />
		<b><?php echo Yii::t("labels", "Tagcash") ?></b>
		</p>
	</i>
	<?php } else { 
				?>
	<i>
		<p><?php echo Yii::t("labels", "Hello there!"); ?></p>

		<p><?php echo Yii::t("labels", $name . " would like to invite you to join its Tagcash Community! Click the link below to register."); ?></p>

		<p>
			<?php if($member){ ?>
				<a href="<?php echo Yii::app()->params['site_url']."/c/".$communityId; ?>"><?php echo Yii::t("labels", "Accept Invitation"); ?></a>
			<?php } ?>
		</p>

		<p><?php echo Yii::t("labels", "If you didn't request this email or have no idea why you received it, please ignore it."); ?></p>

		<p>
			<?php echo Yii::t("labels", "Thanks,"); ?><br /><br />
			<b><?php echo Yii::t("labels", "Tagcash") ?></b>
		</p>
	</i>
		<?php } ?>
	</td>
</tr>