<?php
require Yii::app()->basePath.'/extensions/phpcomposer/vendor/autoload.php';

use Aws\S3\S3Client;
use Aws\Sns\SnsClient;
use Aws\S3\Enum\CannedAcl;

class TAmazon extends CApplicationComponent
{
	public $key;
	public $secret;
	public $bucketRoot;
	public $snsTopicArn;
	public $proxy;
	public $client;
	public $s3Client;
	public $snsClient;

	public function init(){
		if($this->checkClient()){
			$this->client = S3Client::factory(array(
				'key'  => $this->key,
				'secret' => $this->secret,
				'request.options' => array(
					'proxy' => $this->proxy
				),
			));
			$this->s3Client = $this->client;
			$this->snsClient = SnsClient::factory(array(
				'key'  => $this->key,
				'secret' => $this->secret,
				'region' => 'us-east-1',
				'request.options' => array(
					'proxy' => $this->proxy
				),
			));
		}

		return $this->client;
	}

	public function checkClient(){
		if($this->key && $this->secret){
			return true;
		}

		return false;
	}

	public function getClient(){
		return $this->client;
	}

	public function showClient(){
		print_r($this->client);
	}

	public function getPath($relativePath){
		if($relativePath){
			$path = $this->bucketRoot.'/'.$relativePath;
		}
		else{
			$path = $this->bucketRoot;
		}

		return $path;
	}

	public function create($file){
		$path = $this->getPath(NULL);
		$output = $this->client->createBucket(array('Bucket' => $path.'/'.$file));
		return $output;
	}

	public function listObjects($relativePath){
		$result = array();
		$iterator = $this->client->getIterator('ListObjects', array(
			'Bucket' => $this->getPath($relativePath)
		));

		foreach ($iterator as $object) {
			$result[] = $object['Key'];
		}

		return $result;
	}

	public function isObjectExist($relativePath, $file){
		$path = $this->getPath($relativePath);
		$result = $this->client->doesObjectExist($path, $file);

		return $result;
	}

	public function getObject($relativePath, $file){
		$path = $this->getPath($relativePath);
		$result = $this->client->getObject(array(
			'Bucket' => $path,
			'Key'	=> $file
		));

		//echo get_class($result['Body']) . "\n";
		//echo $result['Body'] . "\n";

		return $result;
	}

	public function deleteObject($relativePath, $file){
		$path = $this->getPath($relativePath);
		$result = $this->client->deleteObject(array(
			'Bucket' => $path,
			'Key'	=> $file
		));
		return $result;
	}

	public function uploadObject($relativePath, $fileName, $fileContent, $permissions = CannedAcl::PUBLIC_READ){
		//access to owner only
		if($permissions == 'private'){
			$permissions = NULL;
		}

		if($this->checkClient()){
			$path = $this->getPath($relativePath);
			$result = $this->client->putObject(array(
				'Bucket' => $path,
				'Key'	=> $fileName,
				'Body'   => $fileContent,
				'ACL'	=> $permissions,
			));

			return $result;
		}

		return false;
	}

	public function publishMessage($message, $subject){
		if(is_array($message)){
			$message = json_encode($message);
		}

		if($this->checkClient()){
			$result = $this->snsClient->publish(array(
				'TopicArn' => $this->snsTopicArn,
				//'TargetArn' => $snsTargetArn,
				'Message' => $message,
				'Subject' => $subject,
				'MessageStructure' => '',
			));
			return $result;
		}

		return false;
	}

	function el_crypto_hmacSHA1($key, $data, $blocksize = 64) {
		if (strlen($key) > $blocksize)
			$key = pack('H*', sha1($key));
		$key = str_pad($key, $blocksize, chr(0x00));
		$ipad = str_repeat(chr(0x36), $blocksize);
		$opad = str_repeat(chr(0x5c), $blocksize);
		$hmac = pack('H*', sha1(
						($key ^ $opad) . pack('H*', sha1(
										($key ^ $ipad) . $data
						))
		));
		return base64_encode($hmac);
	}

	function el_s3_getTemporaryLink($bucket, $path, $expires = 1) {
		$accessKey = $this->key;
		$secretKey = $this->secret;
		// Calculate expiry time
		$expires = time() + intval(floatval($expires) * 60);
		// Fix the path; encode and sanitize
		$path = str_replace('%2F', '/', rawurlencode($path = ltrim($path, '/')));
		// Path for signature starts with the bucket
		$signpath = '/' . $bucket . '/' . $path;
		// S3 friendly string to sign
		$signsz = implode("\n", $pieces = array('GET', null, null, $expires, $signpath));
		// Calculate the hash
		$signature = $this->el_crypto_hmacSHA1($secretKey, $signsz);
		// Glue the URL ...
		$url = sprintf('http://%s.s3.amazonaws.com/%s', $bucket, $path);
		// ... to the query string ...
		$qs = http_build_query($pieces = array(
			'AWSAccessKeyId' => $accessKey,
			'Expires' => $expires,
			'Signature' => $signature,
		));
		// ... and return the URL!
		return $url . '?' . $qs;
	}
}
?>