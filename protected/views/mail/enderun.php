<tr>
	<td>
		<img src="http://tagbond.s3.amazonaws.com/uploads/food/ImageEvent.png" height="200" width="590"/>
	</td>
</tr>
<tr>
	<td bgcolor="#ffffff" style="padding:10px">
		<p><?php echo Yii::t("labels", "Hi " . $name . "!"); ?></p>
		<p>You are invited to attend <?php echo $event; ?> Event</p>

		<p><b>Date : </b><?php echo $date.' at '.$opening_time; ?></p>
		<p><b>Venue : </b><?php echo $venue;?></p>

		<p><?php echo Yii::t("labels", "If you didn't request this email or have no idea why you received it, please ignore it.") ?></p
		<p>
			<?php echo Yii::t("labels", "Thank you!"); ?><br /><br />
			<b><?php if($from){
					echo $from;
				}else{
					echo Yii::t("labels", 'Tag77');
				}?></b>
		</p>
	</td>
</tr>
<tr>
	<td bgcolor="#333" style="color:#FFF;">
		<small>Enderun Marketing Service</small>
	</td>
</tr>