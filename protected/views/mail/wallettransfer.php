<?php if($invite){ ?>
	<tr style="background-color: #eb3f3c">
		<td>
			<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i>Tagcash Wallet</i></h1>
		</td>
	</tr>
	<tr>
		<td>
			<p>Hi <?php echo $name; ?></p>

			<p>
				<?php echo $transferFrom . " transferred " . $walletType . " " . $transferAmount . " to this email address."; ?>
			</p>

			<p>
				This transaction will only reflect if you accept it by clicking the <b>Accept</b> button below. If you wish to decline the transaction, just click the <b>Decline</b> button. By declining the transaction, the money will return to <?php echo $transferFrom; ?>
			</p>

			<p>If this transaction hasn't been accepeted after 30 days, the transaction will be automatically declined.</p>

			<p>
				<a href="<?php echo Yii::app()->createAbsoluteUrl('site/login', array('rd'=>'registration','action'=>'nv','from'=>$transferFromId, 'to'=>$transferTo, 'reference_id'=>$referenceId)); ?>" style="cursor:pointer;"><button style="border-radius:2px;padding:8px;color:#fff;background-color:#da4f49;border:0px;">Accept</button></a>
				<a href="<?php echo Yii::app()->createAbsoluteUrl('site/declinetransaction', array('from'=>$transferFromId, 'to'=>$transferTo, 'reference_id'=>$referenceId)); ?>" style="cursor:pointer;"><button style="border-radius:2px;padding:8px;color:#000;background-color:#f5f5f5;border:0px;">Decline</button></a>
			</p>

			<p>
				Thank you,<br><br>
				<b>Tagcash</b>
			</p>
		</td>
	</tr>
<?php } else { ?>
	<tr style="background-color: #eb3f3c">
		<td>
			<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i>Wallet Transaction</i></h1>
		</td>
	</tr>
	<tr>
		<td>
			<p>Hi <?php echo $name; ?></p>

			<p>
				<?php echo $message; ?>
			</p>
			<p>
				This transaction will reflect on your Tagcash Wallet Overview.
			</p>

			<p>
				<?php echo Yii::t('labels', 'Thank you for using our service!'); ?><br><br>
				<b><?php if($from) {
					echo $from;
				} else {
					echo Yii::t('labels', 'Tagcash');
				} ?></b>
			</p>
		</td>
	</tr>
<?php } ?>