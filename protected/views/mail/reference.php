<tr style="background-color: #eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i>Booking Reference Code</i></h1>
	</td>
</tr>
<tr>
	<td>
		<p><?php echo Yii::t("labels", "Hi " . $name . "!"); ?></p>

		<p>Your reference code is</p>

		<p><a href="<?php echo $reference_code; ?>"><?php echo $reference_code; ?></a></p>

		<p>If you didn't request this email or have no idea why you received it, please ignore it.</p>

		<p>Thank you!<br><br>
			<b><?php if($from){
				echo $form;
			} else {
				echo "Tagcash";
			} ?></b>
		</p>
	</td>
</tr>