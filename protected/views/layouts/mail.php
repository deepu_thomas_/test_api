<html>
<head>
	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
	<style>
		@import url(http://fonts.googleapis.com/css?family=PT+Sans);
		body {
			font-family: 'PT Sans', sans-serif;
		}

		a {
			color:#eb3f3c;
		}

		p{
			font-family: 'PT Sans', sans-serif;
			color: #000;
		}
	</style>
</head>
<table cellspacing="0" cellpadding="10" style="color:#303030;font:14px Arial;line-height:1.4em;width:600px;">
	<tbody>
		<?php echo $content; ?>
		<tr>
			<td>
				<hr style="height: 5px; background-color: #eb3f3c; border: none; line-height:0.8em;">
				<center><small color="#9d9d9d"><i>info@tagcash.com</i></small> • <small color="#9d9d9d"><i><a target='_blank' href="http://www.tagcash.com">tagcash.com</a></i></center>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>