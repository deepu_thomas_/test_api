<?php

/**
 * This is the model class for table "site_settings".
 *
 * The followings are the available columns in table 'site_settings':
 * @property integer $id
 * @property string $admin_email
 * @property string $admin_email_password
 * @property string $web_email
 * @property string $web_email_no_reply
 * @property integer $country_id_default
 * @property string $salt1
 * @property string $salt2
 * @property string $ppt_percentage
 */
class SiteSettings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'site_settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('admin_email, admin_email_password, web_email, web_email_no_reply, country_id_default, salt1, salt2', 'required'),
			array('country_id_default', 'numerical', 'integerOnly'=>true),
			array('admin_email, admin_email_password, web_email, web_email_no_reply, salt1, salt2', 'length', 'max'=>255),
			array('ppt_percentage', 'length', 'max'=>4, 'on'=>'changepw'),
			array('cashout_charge_percentage', 'length', 'max'=>4, 'on'=>'changepw'),
			array('conversion_charge_percentage', 'length', 'max'=>4, 'on'=>'changepw'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SiteSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getSettings(){
		$result = new stdClass();
		$sitesettings = SiteSettings::model()->find();
		foreach ($sitesettings as $key => $value) {
			$result->$key = $value;
		}

		return $result;
	}

	public static function changepw($pwd){ 
		$ss = SiteSettings::model()->find();
		$ss->ECPayPassword = $pwd;        
        if ($ss->save()) {
            Rest::sendResponse(200, json_encode("Success", JSON_NUMERIC_CHECK)); 
        } else {
            Rest::sendResponse(403, json_encode("Failed", JSON_NUMERIC_CHECK)); 
        } 
	}
}