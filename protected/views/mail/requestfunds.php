<tr style="background-color: #eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i><b style='text-transform:uppercase'>Request Fund</b></i></h1>
	</td>
</tr>
<tr>
	<td>
		<p><?php echo Yii::t("labels", "Hi there!"); ?></p>
		<p>
			<?php echo "<b>".$name."</b> requested ".$amount." ".$wallet_code; ?>
		</p>
		<?php echo "Approve request on your wallet and enjoy TAGCASH! "; ?>
		<p>
		<p><?php echo "If you didn't request this email or have no idea why you received it, please ignore it."; ?></p>

			<?php echo Yii::t("labels", "Thanks,"); ?><br><br>
			<b><?php echo Yii::t("labels", "Tagcash"); ?></b>
		</p>
	</td>
</tr>