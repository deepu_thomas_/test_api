<?php
error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
ini_set('display_errors', 1);
date_default_timezone_set('Asia/Manila');

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

/* 
included settings
*/
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Tagcash-demo',
	'id' => 'tagbond_mogwai',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.YiiMailer.YiiMailer',
		'ext.YiiMailer.TagbondMail',
		'ext.directmongosuite.components.*',
		'ext.directmongosuite.components.*',
		'ext.firebase.*',
		'ext.easybitcoin.providers.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('172.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		//configure the mongodb connection
		//set the values for server and options analog to the constructor 
		//Mongo::__construct from the PHP manual
		'edms' => array(
			'class'            => 'EDMSConnection',
			'dbName'           => 'tagbond',
			'server'         => 'mongodb://localhost:27017' //default
			//'options'        => array(.....); 
		),

		//manage the httpsession in the collection 'edms_httpsession'
		'session'=>array(
			'class'=>'EDMSHttpSession',
			//set this explizit if you want to switch servers/databases
			//See below: Switching between servers and databases
			//'connectionId'=>'edms',
			//'dbName'=>'testdb',
			// 'savePath' => '/var/www/development/tagbond/cookie',
			'cookieMode' => 'allow',
			'cookieParams' => array(
				'path' => '/',
				//'domain' => '.beta.tagbond.com',
				'httpOnly' => true,
			),
			'autoStart'=>true,
		),

		'id' => 'tagbond_cookie_share',
 
		//manage the cache in the collection 'edms_cache'
		'cache' => array(
			'class'=>'EDMSCache',    
			//set to false after first use of the cache to increase performance
			'ensureIndex' => true,
			//Maybe set connectionId and dbName too: see Switching between servers and databases 
		),

		//uses the collection 'edms_authmanager' for the authmanager
		'authManager'=>array(
			'class'=>'EDMSAuthManager',
		),
		
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'gii'=>'gii',
				'gii/<controller:\w+>'=>'gii/<controller>',
				'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',


				'rfid/details/<id>/*'=>'rfid/details', //For negative rfid tags
				'rfid/bind/<id>/*'=>'rfid/bind', //For negative rfid tags

				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			'showScriptName'=> false
		),

		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		// MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=contact_test',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
		),
            	

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'public/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

		

		
	),


	'behaviors' => array(
		'edms' => array(
		'class'=>'EDMSBehavior',

		)
	),




	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// global
		//'proxy'=>'172.17.1.1:3128',
		'adminEmail'=>'info@tagcash.com',
		
		'salt1'=>'1',
		'salt2'=>'2',
		
	),
);