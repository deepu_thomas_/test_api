<?php

/**
 * This is the model class for table "module_list_via_country_or_wallet".
 *
 * The followings are the available columns in table 'module_list_via_country_or_wallet':
 * @property integer $id
 * @property integer $country_id
 * @property integer $wallet_id
 * @property integer $module_id
 * @property string $type
 *
 * The followings are the available model relations:
 * @property ModuleList $module
 * @property Countries $country
 */
class ModuleListViaCountryOrWallet extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'module_list_via_country_or_wallet';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('module_id, type', 'required'),
            array('country_id, wallet_id, module_id', 'numerical', 'integerOnly'=>true),
            array('type', 'length', 'max'=>1),
            array('country_id,wallet_id','existCheck'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, country_id, wallet_id, module_id, type', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'module' => array(self::BELONGS_TO, 'ModuleList', 'module_id'),
            'country' => array(self::BELONGS_TO, 'Countries', 'country_id'),
            'wallet' => array(self::BELONGS_TO, 'WalletTypes', 'wallet_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'country_id' => 'Country',
            'wallet_id' => 'Wallet',
            'module_id' => 'Module',
            'type' => 'User Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('country_id',$this->country_id);
        $criteria->compare('wallet_id',$this->wallet_id);
        $criteria->compare('module_id',$this->module_id);
        $criteria->compare('type',$this->type,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ModuleListViaCountryOrWallet the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function existCheck($attribute){
        if(!$this->country_id && !$this->wallet_id){
            $this->addError($attribute,'missing '.$attribute);
        }
    }
    /*
     * 
     * returns active modules for a given community or user 
     * returns all active modules for country 174 or have php wallet  with filter user type   
     * 
     */
    public static function getModuleList($userId,$userType,$countryId){
            $searchArr=array('balance_id'=>$userId,'balance_type'=>$userType,'wallet_type_id'=>1);
            $phpWallet=  WalletBalances::model()->findByAttributes($searchArr);
            $criteria=new CDbCriteria();
            if($countryId == 174 || $phpWallet){ //fetching all active modules
             $criteria->condition='(country_id = :country_id or wallet_id = :wallet_id) and type = :type';
             $criteria->params=array(':country_id'=>174,':wallet_id'=>1,':type'=>$userType);
            }else{
             $criteria->condition='(country_id = :country_id) and type = :type';  
             $criteria->params=array(':country_id'=>$countryId,':type'=>$userType);
            }
           $moduleList= ModuleListViaCountryOrWallet::model()->with('module')->findAll($criteria);
           $modulesArr=array();
           foreach($moduleList as $module){
               if($module->module->active == 'Y'){
                   $modulesArr[]=$module->module->name;
               }
           }
//           if(count($modulesArr)< 1){ // default for 
//               $modulesArr[]='Cash In';
//           }
           return $modulesArr;
    }
}
