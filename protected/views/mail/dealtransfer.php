<tr style="background-color:#eb3f3c;">
	<td>
		<h1 style="color:#FFF; margin-bottom:0px; margin-top: 50px;"><i>Someone wants to give a tagvert to you!</i></h1>
	</td>
</tr>
<tr>
	<td>
		<p><?php echo Yii::t("labels", "Hi!"); ?></p>

		<p><?php echo $sender." wants to transfer the tagvert '".$dealTitle."' to you."; ?></p>

		<p>
			<?php echo "Sign up now on <a href='tagbond.com/site/login?email=".$email."'>Tagbond</a> and enjoy this wonderful tagvert."; ?>
		</p>

		<p><?php echo "If you didn't request this email or have no idea why you received it, please ignore it."; ?></p>

		<p>
			<?php echo "Thanks,"; ?><br /><br />
			<b><?php echo "Tagbond"; ?></b>
		</p>
	</td>
</tr>