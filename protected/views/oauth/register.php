<div id="login-page">
	<div class="navbar navbar-fixed-top redbg" style="height:.2em;">
	</div>


	<div class="mycontainer logincontainer clearfix">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'loginPage-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
			'htmlOptions' => array(
				'class' => 'form'
			)
		));?>

		<div class="row-fluid">
			<div class="navbar-inner redbg" style="height:3.6em;">
				<div class="span4">
					<a href="#" class="logo clearfix">
						<img src="<?php echo Yii::app()->createUrl("images/logo.png"); ?>" alt="Tagcash Logo"/>
					</a>
				</div>
				<div class="span8">
					<h4>Signup for Tagcash</h4>
				</div>
			</div>
		</div>

		<?php
		if(Yii::app()->user->hasFlash("error")){
			echo "<div class='alert alert-error'>";
			echo Yii::app()->user->getFlash("error");
			echo "</div>";
		}
		?>

		<div class="row-fluid tBreak">
			<div class="span4">
				<label class="control-label"><?php echo $form->label($modelRegister,'username'); ?></label>
			</div>
			<div class="span8">
				<?php echo $form->emailField($modelRegister,'user_email',array('placeholder'=>'Email','class'=>'Form registerEmail loginL')); ?>
				<?php echo $form->error($modelRegister,'user_email'); ?>
			</div>
		</div>

		<div class="row-fluid tBreak">
			<div class="span4">
				<label class="control-label"><?php echo $form->label($modelRegister,'user_password'); ?></label>
			</div>
			<div class="span8">
				<?php echo $form->passwordField($modelRegister,'user_password', array('placeholder'=>'Password','class'=>'Form registerPassword loginP')); ?>
				<?php echo $form->error($modelRegister,'user_password'); ?>
			</div>
		</div>

		<div class="row-fluid tBreak">
			<div class="span4">
				<label class="control-label"><?php echo $form->label($modelRegister,'user_firstname'); ?></label>
			</div>
			<div class="span8">
				<?php echo $form->textField($modelRegister,'user_firstname',array('placeholder'=>Yii::t('labels','First Name'),'class'=>'Form loginL2')); ?>
				<?php echo $form->error($modelRegister,'user_firstname'); ?>
			</div>
		</div>

		<div class="row-fluid tBreak">
			<div class="span4">
				<label class="control-label"><?php echo $form->label($modelRegister,'user_lastname'); ?></label>
			</div>
			<div class="span8">
				<?php echo $form->textField($modelRegister,'user_lastname',array('placeholder'=>Yii::t('labels','Last Name'),'class'=>'Form loginL2')); ?>
				<?php echo $form->error($modelRegister,'user_lastname'); ?>
			</div>
		</div>
		
		<div class="row-fluid">
			<center>
				<input type="checkbox" value="1" id="Users_agreement" name="Users[agreement]"> &nbsp; I agree to Tagcash's <a id="click_terms" data-toggle="modal" href="#terms">Terms and Privacy</a>
				<?php echo $form->error($modelRegister,'agreement'); ?>
				<p></p><div style="display:none" id="Users_agreement_em_" class="errorMessage"></div><p></p>
			</center>
		</div>
		<div class="row-fluid tBreak">
			<div class="span4"></div>
			<div class="span8">
				<input type="submit" value="Sign Up" name="btnRegister" class="btn btn-danger btn-rounded btn-large width220">
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<div class="modal hide fade in" id="terms" style="display: block;" aria-hidden="false">
  	<div class="modal-header">
    	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
    	<h4>Terms and Privacy</h4>
  	</div>
  	<div class="modal-body">
    	<b>TAGCASH Terms of Service</b>
	<p>This Terms of Service Agreement (the “Agreement”) describes the terms and conditions on which TAGCASH (hereinafter “TAGCASH”, “Tagcash”, “tagcash”, “we” or “our company”) offer Services (as defined below) to you (“User” or “You”). By registering for or using TAGCASH Services, or accessing or browsing this website, you agree to be bound by the following terms and conditions whether or not you are a registered member of TAGCASH. Your use of the TAGCASH Services may also be governed by the TAGCASH Privacy Policy and other agreements as may be applicable to a particular TAGCASH Service, which are hereby incorporated by reference into this Agreement. We retain the right at our sole discretion to deny access to anyone to our website(s) and the services we offer, at any time and for any reason, including, but not limited to, violation of any term of this Agreement.</p>

	<b>For the purposes of this Agreement:</b>
	<p>“User” or “you” means the individual or business entity (including its employees and agents) that is using or registering to use the Services or accessing or browsing this website;</p>

	<p>“TAGCASH Services” or “Services” means those electronic or interactive services offered by TAGCASH.
	</p><br>

	<p><i>PLEASE READ THESE TERMS OF SERVICE CAREFULLY AS THEY CONTAIN IMPORTANT INFORMATION REGARDING YOUR LEGAL RIGHTS, REMEDIES, AND OBLIGATIONS.</i></p>

	<b>Eligibility for TAGCASH Services</b>
	<p>Our Services are available only to individuals of at least eighteen years old and business entities (including but not limited to sole proprietorships) in good legal standing that can form legally binding contracts and are entitled to subscribe to and access our services under applicable law. A User who is an individual hereby represents and warrants that he/she is at least eighteen years old. A User which is a business entity hereby represents and warrants that it is duly licensed to do business and is in good legal standing in the jurisdictions in which it does business (during the term of this Agreement), that it is not a competitor of TAGCASH, and that the person agreeing to this Agreement for such User is at least eighteen years of age and otherwise capable of and authorized to enter binding contracts for such User.</p>

	<b>TAGCASH Services</b>
	<p>Created as a platform for easy transfer and payments using digital wallets, including multiple fiat currencies, eWallets and cryptocurrencies (Bitcoin and Tagcoin - service coming soon). It also handles reporting of Tagbond reward points.</p>

	<p>The Tagbond reward points in the account of each User may increase with every purchase made (depending on a Community’s Tagvert). A User’s Tagbond reward points cannot be transferred to other Users.</p>

	<p>Aside from using Tagbond Reward Points to purchase goods and services (if applicable) from participating Merchants, Users may also make use of cryptocurrencies such as Bitcoin and Tagcoin (coming soon).</p>

	<p>The fees for using the TAGCASH Services are set out in <a href="http://www.tagcash.com/fees">Tagcash Fees</a></p>

	<p>Subject to the terms and conditions of this Agreement, you may access and use the TAGCASH Services for your own personal non-commercial use. TAGCASH reserves the right to change or discontinue any of the Services at any time.</p>

	<p>TAGCASH likewise reserves the right to exercise whatever lawful means it deems necessary to prevent unauthorized use of the TAGCASH Services, including, but not limited to, technological barriers, IP mapping, and/or directly contacting your Internet service provider regarding such unauthorized use.</p>

	<b>Ownership</b>
	<p>The TAGCASH Services are owned and operated by TAGCASH. You acknowledge that all materials (except those uploaded by you and other users) provided on our websites, including but not limited to information, documents, products, logos, graphics, sounds, GUI, software, and services (collectively “Materials”), are provided either by TAGCASH or by their respective third party authors, developers, and vendors (collectively “Third Party Providers”) and the underlying intellectual property rights are owned by TAGCASH and/or its Third Party Providers. Elements of our websites are protected by intellectual property laws and may not be copied or imitated in whole or in part. TAGCASH, the TAGCASH logo, and other TAGCASH products referenced herein are trademarks of TAGCASH, and may be registered in certain jurisdictions. All other product names, company names, marks, logos, and symbols may be the trademarks of their respective owners. All rights to the same are hereby reserved.</p>

	<b>User Information</b>
	<p>User represents and warrants that the information it provides in TAGCASH contact information forms is true, accurate, current, and complete. User agrees to maintain and update this information to ensure that it is true, accurate, current, and complete. If, at any time, any information provided by User is untrue, inaccurate, not current, or incomplete, TAGCASH will have the right to suspend, restrict, or terminate User’s account and this Agreement.</p>

	<b>User Account</b>
	<ol>
		<li>
			<p><i>Membership Accounts</i></p>
			<p>User may become a registered member (“Member”) by entering into a membership agreement with TAGCASH or other relevant agreements of TAGCASH from time to time. Members are limited to one free account per person or business entity. Your TAGCASH account is solely for your own personal use and benefit. Notwithstanding the above, you acknowledge that TAGCASH reserves the right to charge for any portion of the TAGCASH Services and to change its fees (if any) from time to time in its sole discretion.</p>

			<p>You are solely responsible for your interactions with other Members. TAGCASH reserves the right, but has no obligation, to resolve or arbitrate disputes between you and other Members, including, but not limited to, the suspension or termination of the account of either or both Members.</p>
		</li>

		<li>
			<p><i>Authorized Users</i></p>
			<p>You may designate persons to act as your agents to use the Services, provided that each designated person has the legal capacity to enter into binding contracts for you. Furthermore, you represent and warrant that each person who registers under your account is your authorized agent (an “Authorized User”) who has such legal capacity.</p>
		</li>

		<li>
			<p><i>Responsibility for Access</i></p>
			<p>You are solely responsible for maintaining the confidentiality of your access information (i.e., account IDs and passwords) and for restricting access to your computer, and you agree to accept responsibility for all activities, actions, incidents, and damages that occur under your account or password.</p>
		</li>

		<li>
			<p><i>Notification of Unauthorized Use</i></p>
			<p>If you have reason to believe that your account is no longer secure (e.g., in the event of a loss, theft, or unauthorized disclosure or use of your account ID, password, or any credit, debit, or charge card number, if applicable), then you agree to immediately notify TAGCASH.</p>
		</li>

		<li>
			<p><i>Identity Authentication</i></p>
			<p>You authorize TAGCASH, directly or through third parties, to make any inquiries we consider necessary to validate your identity. This may include asking you for further information, requiring you to provide your date of birth, a taxpayer identification number, and other information that will allow us to reasonably identify you, requiring you to take steps to confirm ownership of your email address or financial instruments, ordering a credit report, or verifying your Information against third party databases or through other sources. We may also ask to see your driver’s license or other identifying documents at any time. TAGCASH reserves the right to close, suspend, or limit access to your account and/or the TAGCASH Services in the event we are unable to obtain or verify these information.</p>
		</li>
	</ol>

	<b>TAGCASH's Intellectual Property Policy</b>
	<p>TAGCASH respects artist and content owner rights. Thus, it is TAGCASH's policy to respond to notices of alleged infringement. TAGCASH will promptly terminate without notice any User’s access to the Services if that User is determined in the sole discretion of TAGCASH to be a “repeat infringer.” For purposes of this provision, a repeat infringer is a User who has been notified by TAGCASH of infringing activity violations more than twice.</p>

	<p>You agree not to use the TAGCASH site or the Services to transmit, route, provide connections to, or store any material that infringes copyrighted works or otherwise violates or promotes the violation of the intellectual property rights of any third party.</p>

	<b>Acceptable Use</b>
	<p>You agree to comply with all applicable local, national and foreign laws, treatises, and regulations in connection with your use of the Services. By using the TAGCASH Services, you further agree not to:</p>
	<ol>
		<li>use the Services in any manner not permitted by this Agreement;</li>
		<li>circumvent, disable, or interfere with security-related features of the Services or features that prevent or restrict use or copying of any content therein;</li>
		<li>rent, lease, loan, sell, resell, sublicense, distribute, or otherwise transfer the Services granted herein;</li>
		<li>delete the copyright or other proprietary rights on the TAGCASH Services;</li>
		<li>make unsolicited offers, advertisements, proposals, or send junk mail or spam to other Members or Users. This includes, but is not limited to, unsolicited advertising, promotional materials, or other solicitation material, bulk mailing of commercial advertising, chain mail, informational announcements, charity requests, and petitions for signatures;</li>
		<li>use the TAGCASH Services for any illegal purpose, or in violation of any local, national, or foreign law, including, without limitation, laws governing intellectual property and other proprietary rights, and data protection and privacy;</li>
		<li>reverse engineer, decompile, disassemble, or otherwise attempt to discover the source code of the TAGCASH Services or any part thereof, except and only to the extent that such activity is expressly permitted by applicable law notwithstanding this limitation;</li>
		<li>attempt to gain access to any account or computer resource not belonging to you without authorization from the owner (e.g., hacking);</li>
		<li>facilitate any viruses, trojan horses, worms, or other computer programming routines that may damage, detrimentally interfere with, or surreptitiously intercept or expropriate any system, data, or information;</li>
		<li>interfere with the use or enjoyment of the TAGCASH network or Services by other Users or Authorized Users;</li>
		<li>hold TAGCASH, its suppliers, officers, directors, employees, agents, affiliates, or shareholders up to public scorn, ridicule, or defamation;</li>
		<li>remove, modify, disable, block or obscure, or otherwise impair any advertising in connection with the TAGCASH Services;</li>
		<li>circumvent any TAGCASH policy or determination about your account such as, but not limited to, temporary or indefinite suspensions by attempting to create a new or additional TAGCASH account, creating a new or additional TAGCASH account with information belonging to others, using another person’s TAGCASH account, or any other similar acts;</li>
		<li>refuse to cooperate in an investigation or provide confirmation of your identity or any information which you provided to TAGCASH;</li>
		<li>use the Services in a manner that results in or may result in complaints, claims, or any other liability to TAGCASH, other Users, third parties, or you;</li>
		<li>take any action that may cause TAGCASH to lose any of the services of its internet service providers, payment processors, or other suppliers;</li>
		<li>control an account that is linked to another account that is committing any of the foregoing acts;</li>
		<li>encourage or instruct any other individual or entity to do any of the foregoing or to violate any term of this Agreement;</li>
		<li>do anything analogous or similar to the foregoing.</li>
	</ol>

	<b>Disclaimer</b>
	<h4>YOU EXPRESSLY UNDERSTAND AND AGREE THAT:</h4>
	<ol>
		<li><p>YOUR USE OF THE TAGCASH SERVICES IS AT YOUR SOLE RISK. TAGCASH SERVICES ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. TAGCASH, ITS SUPPLIERS, OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, AND ASSOCIATES TO THE FULLEST EXTENT PERMITTED BY LAW, DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE TAGCASH WEBSITE AND SERVICES AND YOUR USE THEREOF, INCLUDING BUT NOT LIMITED TO WARRANTIES OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABILITY, AND NON-INFRINGEMENT OF PROPRIETARY OR THIRD PARTY RIGHTS. TAGCASH, ITS SUPPLIERS, OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, AFFILIATES, AND SHAREHOLDERS MAKE NO WARRANTIES ABOUT THE ACCURACY, RELIABILITY, COMPLETENESS, OR TIMELINESS OF OUR SERVICES, SOFTWARE, OR CONTENT;</p></li>

		<li><p>TAGCASH MAKES NO WARRANTY AND ASSUMES NO LIABILITY OR RESPONSIBILITY (I) THAT THE SERVICES WILL MEET YOUR REQUIREMENTS, (II) THAT THE SERVICES WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE, (III) THAT THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICES WILL BE ACCURATE OR RELIABLE, (IV) THAT THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE SERVICES WILL MEET YOUR EXPECTATIONS, (V) THAT ANY ERRORS IN THE SOFTWARE WILL BE CORRECTED; (VI) FOR ANY PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICES OR WEBSITE, (VII) FOR ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, (VIII) FOR ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR WEBSITE, (IX) FOR ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH OUR WEBSITE BY ANY THIRD PARTY, AND/OR (X) FOR ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA OUR WEBSITES;</p></li>

		<li><p>ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICES IS DONE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL;</p></li>

		<li><p>NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM US OR THROUGH OR FROM THE SERVICE SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THESE TERMS AND CONDITIONS; AND</p></li>

		<li><p>TAGCASH SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, EVEN IF TAGCASH, ITS SUPPLIERS, OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, AFFILIATES AND SHAREHOLDERS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p></li>
		</ol>

		<p>Some jurisdictions do not allow for the exclusion of certain warranties or conditions or the limitation or exclusion of liability for loss or damage caused by negligence, breach of contract or breach of implied terms, or incidental or consequential damages, so the limitations or exclusions of liability in this Agreement above may not apply to you.</p>

		<b>Indemnity</b>
		<p>You agree to defend, indemnify, and hold harmless TAGCASH, its affiliates, and/or their respective suppliers, officers, directors, employees and agents, from and against any reversals, chargebacks, claims, fees, actions or demands, recoveries, losses, damages, fines, penalties, including without limitation reasonable legal fees, alleging or resulting from your use the Services, or your violation of the rights of a third party or your breach of this Agreement or your warranties herein or other TAGCASH policies, terms and conditions.</p>

		<b>Limitation of Liability</b>
		<p>Your use of the Services is at your own risk. If you are dissatisfied with any aspect of our Services or with these terms and conditions, or any other rules or policies, your sole remedy is to discontinue use of the Services or termination of your membership. You expressly understand and agree that TAGCASH shall not be liable for any direct, indirect, incidental, special, consequential exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data, or other intangible losses (even if we have been advised of the possibility of such damages), resulting from: </p><ol><li type="a">the use or the inability to use the Services; </li><li type="a">the cost of procurement of substitute goods and services resulting from any goods, data, information, or services purchased or obtained or messages received or transactions entered into through or from the Services; </li><li type="a">unauthorized access to or alteration of your transmissions or data; </li><li type="a">statements or conduct of any third party on the service; or </li><li type="a">any other matter relating to the Services.</li></ol><p></p>

		<p><b>Modifications to Agreements, Policies, or to our Services</b></p>
		<p>We reserve the right to change this Agreement at any time without notice. We also reserve the right at any time to modify or discontinue the Services, temporarily or permanently, with or without notice to you. You agree that we shall not be liable to you or any third party for any modification, suspension, or discontinuance of the Services. Further, you acknowledge that TAGCASH may change applicable subscription fees in Schedule “A” at any time without notice. Changes in subscription fees will take effect on expiration of any existing subscription.</p>

		<b>Termination</b>
		<ol>
			<li type="a">Suspension or Termination by TAGCASH

			<p>Without prejudice to other remedies which TAGCASH may have, TAGCASH may immediately issue a warning, suspend (i.e., lock out access and operation of Services for User) or restrict either temporarily or indefinitely, or terminate User’s account and refuse to provide Services to User if: (a) TAGCASH believes that User has violated or acted inconsistently with this Agreement, or any of our policies; or (b) User has failed to pay fees or other payments due to TAGCASH; or (c) TAGCASH is unable to verify or authenticate any information User provides to TAGCASH; or (d) TAGCASH believes that User’s actions may cause legal liability for User, TAGCASH's other clients, or TAGCASH.</p>

			<p>TAGCASH may also in its sole discretion and at any time discontinue providing the Services, or any part thereof, with or without notice. You agree that any termination of your access to the Services under any provision of these terms and conditions may be effected without prior notice, and acknowledge and agree that TAGCASH may immediately deactivate, archive, or delete your account and all related information and data and/or any further access to such data or the Service. Further, you agree that TAGCASH shall not be liable to you or any third-party for any termination of your access to the Services.</p></li>

			<li type="a"> Effect of Termination

			<p>Upon termination of this Agreement by either User or TAGCASH, all of User’s rights under this Agreement, and TAGCASH's provision of Services, shall terminate immediately.</p></li>

		</ol>

		<b>Third Party Websites and Information</b>
		<p>Our website(s) may provide hyperlinks to third party websites or access to third party content. TAGCASH does not control, endorse, guarantee, or assume responsibility for any content, product, or service found in such third party websites through our websites or any hyperlinked website or featured in any banner or other advertising. You agree that TAGCASH is not responsible for any content, associated links, resources, or services associated with a third party website and TAGCASH will not be a party to or in any way be responsible for monitoring any transaction between you and third-party providers of products or services. As with the purchase of a product or service through any medium or in any environment, you should use your best judgment and exercise caution where appropriate.</p>
		<p>You further agree that TAGCASH shall not be liable for any loss or damage of any sort associated with your use of third party content. Links and access to these third party websites are provided for your convenience only.</p>

		<b>Arbitration</b>
		<p>YOU AND TAGCASH AGREE THAT, EXCEPT AS MAY OTHERWISE BE PROVIDED IN REGARD TO SPECIFIC SERVICES ON THE SITE IN ANY SPECIFIC TERMS APPLICABLE TO THOSE SERVICES, OR UPON REQUEST BY TAGCASH FOR RESOLUTION AT ANY OTHER FORUM AT TAGCASH’S SOLE DISCRETION, THE SOLE AND EXCLUSIVE FORUM AND REMEDY FOR ANY AND ALL DISPUTES AND CLAIMS RELATING IN ANY WAY TO OR ARISING OUT OF THIS AGREEMENT, THE SITE, AND/OR THE SERVICE (INCLUDING YOUR VISIT TO OR USE OF THE SITE AND/OR THE SERVICE) SHALL BE FINAL AND BINDING ARBITRATION, except that to the extent that either of us has in any manner infringed upon or violated or threatened to infringe upon or violate the other party's patent, copyright, trademark, or trade secret rights, or you have otherwise violated any of the user policy or conduct rules set forth above then the parties acknowledge that arbitration is not an adequate remedy at law and that injunctive or other appropriate relief may be sought. Arbitration under this Agreement shall be conducted by the Hong Kong International Arbitration Centre. The language of the arbitration shall be English. The arbitrator's award shall be binding and may be entered as a judgment in any court of competent jurisdiction. To the fullest extent permitted by applicable law and except as otherwise approved by TAGCASH, NO ARBITRATION OR CLAIM UNDER THESE TERMS OF USE SHALL BE JOINED WITH ANY OTHER ARBITRATION OR CLAIM, INCLUDING ANY ARBITRATION OR CLAIM INVOLVING ANY OTHER CURRENT OR FORMER USER OF THE SERVICE, AND NO CLASS ARBITRATION PROCEEDINGS SHALL BE PERMITTED.</p>

		<b>Miscellaneous</b>
		<p>These terms and conditions will be governed by and construed in accordance with the laws of the Philippines, excluding that body of law governing conflict of laws. Any legal action or proceeding not subject to arbitration, relating to or arising out of these Terms or your use of our websites will be brought in the proper courts in Makati City, and you submit to the venue and personal jurisdiction of such court and waive all defences of lack of personal jurisdiction and forum non conveniens with respect to venue and jurisdiction in Makati City. If any provision of this Agreement is held to be invalid or unenforceable, such provision will be enforced to the greatest extent possible and the remaining provisions will remain in full force and effect. Headings are for reference purposes only and in no way define, limit, construe, or describe the scope or extent of such section. TAGCASH's failure to act with respect to a breach by User or others does not waive TAGCASH's right to act with respect to subsequent or similar breaches. No action by User arising under this Agreement may be brought at any time more than twelve (12) months after the facts occurred upon which the cause of action arose.</p>

		<ol>
<li>Assignment

<p>User may not assign any of its rights, or delegate any of its duties under this Agreement, and any attempted assignment will be null and void. TAGCASH may assign any of its rights or obligations without notice and/or restriction.</p></li>


<li>Force Majeure

<p>Operation of our Services may be interfered with by numerous factors outside of our control and we shall not be liable to you for any delay or failure in performance under this Agreement resulting directly or indirectly from causes beyond TAGCASH's control.</p></li>

<li>Interpretation

<p>If any provision of this Agreement is held to be invalid or unenforceable, such provision shall be struck out, as narrowly as possible, and the remaining provisions shall be enforced. Headings are for reference purposes only and in no way define, limit, construe or describe the scope or extent of such section. This Agreement is prepared and executed in the English language. The English language version shall govern the parties' relationship. Any translation of this Agreement into other languages shall be for informational purposes and shall have no legal effect.</p>
</li>
<li>TAGCASH Confidential Information

<p>You represent and warrant to TAGCASH that (a) you are not a competitor of TAGCASH, (b) you shall keep publicly unannounced information and materials pertaining to TAGCASH, pre-release software, testing or testing procedures strictly confidential, and (c) you shall not use any information gained from access to the TAGCASH websites or use of the TAGCASH Services to compete with TAGCASH in its business.</p></li>

<li>Entire Agreement

<p>Except for other agreements or terms appearing on our websites, this Agreement sets forth the entire understanding and agreement between us with respect to the subject matter hereof.</p></li>
</ol>

<b>Additional Information</b>
<p>Any questions relating to our Legal Agreements and Policies may be directed through <a href="mailto:info@tagcash.com">email</a>.</p>
  	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#terms').hide();	
	$("#click_terms").click(function() {
        $('#terms').show();	
    });
    $(".close").click(function() {
        $('#terms').hide();	
    });
});
</script> 
