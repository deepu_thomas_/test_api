<?php

class Matchmaker extends CFormModel {

    public $like;
    public $dislike;
    public $users;
    public $userId;

    public function updateMatches($operation) {
        if (is_array($this->users)) {

            $like = array();
            $dislike = array();
            $users = array();
            $dislikeUsers = array();
            $likeUsers = array();

            $collection = Yii::app()->edmsMongoCollection('matchmaker');
            $data = $collection->findOne(array(
                "_id" => $this->userId
            ));

            //if data exists, update
            if ($data) {
                $like = $data['like'];
                $dislike = $data['dislike'];
                foreach ($this->users as $user) {
                    //check valid user
                    $userDetails = Users::model()->findByPk($user);
                    if ($operation == 'like') {
                        //check already in like
                        if ($userDetails && !in_array($user, $like)) {
                            $users[] = strval($user);
                        }
                        if (in_array($user, $dislike)) {
                            $dislikeUsers[] = strval($user);
                        }
                    }

                    if ($operation == 'dislike') {
                        //check already in dislike
                        if ($userDetails && !in_array($user, $dislike)) {
                            $users[] = strval($user);
                        }
                        if (in_array($user, $like)) {
                            $likeUsers[] = strval($user);
                        }
                    }
                }
                if ($operation == 'like') {
                    $dislike = array_values(array_diff($dislike, $dislikeUsers));
                    $like = array_merge($like, $users);
                }
                if ($operation == 'dislike') {
                    $like = array_values(array_diff($like, $likeUsers));
                    $dislike = array_merge($dislike, $users);
                }
                $collection->update(array(
                    "_id" => $this->userId
                        ), array('$set' => array("like" => $like, 'dislike' => $dislike)));
                return $data = $collection->findOne(array(
                    "_id" => $this->userId
                ));
            } else {
                $parsedData = new stdClass();
                foreach ($this->users as $user) {
                    $userDetails = Users::model()->findByPk($user);
                    if ($userDetails) {
                        $users[] = strval($user);
                    }
                }

                $parsedData->_id = $this->userId;

                if ($operation == 'like') {
                    $parsedData->like = $users;
                    $parsedData->dislike = array();
                }

                if ($operation == 'dislike') {
                    $parsedData->like = array();
                    $parsedData->dislike = $users;
                }

                $collection->insert($parsedData);
                return $data = $collection->findOne(array(
                    "_id" => $this->userId
                ));
            }
        }
    }

    public static function formatUserData($user, $userId = null) {
        $data['user_id'] = $user['_id'];
        $data['first_name'] = $user['first_name'] ? $user['first_name'] : '';
        $data['last_name'] = $user['last_name'] ? $user['last_name'] : '';

        if ($userId)
            $data['nickname'] = Nicknames::getNickname($user['_id'], $userId);

        $data['dob'] = substr($user['dob'], 0, 4) . '-' . substr($user['dob'], 4, 2) . '-' . substr($user['dob'], 6, 2);
        $data['dob'] = $data['dob'] ? $data['dob'] : '';
        if ($user['gender'] == 1)
            $gender = 'male';
        else if ($user['gender'] == 2)
            $gender = 'female';
        else if ($user['gender'] == 3)
            $gender = 'ts';
        else
            $gender = '';

        $data['gender'] = $gender;
        $data['city'] = $user['city'] ? $user['city'] : '';
        $data['country'] = Countries::getCountryName(intval($user['country']));
        $data['location'] = $user['location'] ? $user['location'] : array();
        $data['keyword'] = $user['keyword'] ? $user['keyword'] : array();
        $data['visibility'] = (isset($user['visibility']) && $user['visibility'] == 0) ? 'invisible' : 'visible';
        return $data;
    }

    public static function getMatchmakerDetails($userId, $short = false) {
        $collection = Yii::app()->edmsMongoCollection('matchmaker');
        $data = $collection->findOne(array(
            "_id" => $userId
        ));
        $data['visibility'] = (isset($data['visibility']) && $data['visibility'] == 0) ? 'invisible' : 'visible';
        if ($short)
            unset($data['_id'], $data['like'], $data['dislike']);

        return $data;
    }

    public static function updateMatchmakerDetails($user, $status = null) {
        $update = new stdClass();

        //matchmaker on or off - on =1 off=0
        if (!is_null($status)) {
            $update->matchmaker = intval($status);
        }

        $update->nickname = $user->user_nickname ? $user->user_nickname : $user->user_firstname;
        $update->first_name = $user->user_firstname;
        $update->last_name = $user->user_lastname;
        $update->gender = intval($user->user_gender);
        $dob = str_replace('-', '', $user->user_dob);
        $update->dob = intval($dob);
        $update->city = $user->user_city;
        $update->country = intval($user->country_id);
        $update->keyword = explode(',', $user->keyword);

        if (Common::isValidLatitude($user->latitude) && Common::isValidLongitude($user->longitude))
            $update->location = array(floatval($user->longitude), floatval($user->latitude));
        else
            $update->location = array();
        $collection = Yii::app()->edmsMongoCollection('matchmaker');
        $data = $collection->findOne(array(
            "_id" => $user->id
        ));

        //if data already in matchmaker : update
        if ($data) {
            unset($update->location);
            if (Common::isValidLatitude($user->latitude) && Common::isValidLongitude($user->longitude))
                $update->location = array(floatval($user->longitude), floatval($user->latitude));
            else {
                if (!isset($data['location'])) {
                    $update->location = array();
                }
            }
            $status = $collection->update(array(
                "_id" => $user->id), array('$set' => $update));
        }
        //new to matchmaker : insert
        else {
            $update->like = array();
            $update->dislike = array();
            $update->_id = $user->id;
            $status = $collection->insert($update);
        }
    }

    public static function matchmakerSearch($userId, $params) {
        if (!isset($params['offset']) || !is_numeric($params['offset']))
            $pageOffset = 0;
        else
            $pageOffset = $params['offset'];

        if (!isset($params['count']) || !is_numeric($params['count']))
            $pageCount = 100;
        else
            $pageCount = $params['count'];

        $collection = Yii::app()->edmsMongoCollection('matchmaker');

        if ($params['gender']) {
            $gender = json_decode($params['gender']);
            if (is_array($gender)) {
                foreach ($gender as $gen) {
                    if ($gen == 'male')
                        $or[] = array('gender' => 1);
                    else if ($gen == 'female')
                        $or[] = array('gender' => 2);
                    else if ($gen == 'ts')
                        $or[] = array('gender' => 3);
                }
                $query['$or'] = $or;
            }
            else {
                if ($params['gender'] == 'male') {
                    $query['gender'] = 1;
                } else if ($params['gender'] == 'female') {
                    $query['gender'] = 2;
                } else if ($params['gender'] == 'ts') {
                    $query['gender'] = 3;
                } else
                    $query['gender'] = $params['gender'];
            }
        }
        else {
            $query['gender'] = array('$ne' => intval(0), '$exists' => true);
        }
        if ($params['age']) {
            if (Common::isJson($params['age'])) {
                $age = json_decode($params['age'], true);
                $searchDate1 = date('Ymd', strtotime(date('Y-m-d') . ' - ' . intval($age[0]) . ' year'));
                $searchDate2 = date('Ymd', strtotime(date('Y-m-d') . ' - ' . intval($age[1]) . ' year'));
                $query["dob"] = array('$lte' => intval($searchDate1),
                    '$gte' => intval($searchDate2));
            } else {
                $searchDate = date('Ymd', strtotime(date('Y-m-d') . ' - ' . intval(substr($params['age'], 2)) . ' year'));

                if (substr($params['age'], 0, 2) == 'lt') {
                    $query["dob"] = array('$gt' => intval($searchDate));
                }

                if (substr($params['age'], 0, 2) == 'gt') {
                    $query["dob"] = array('$lt' => intval($searchDate));
                }

                if (substr($params['age'], 0, 2) == 'eq') {
                    $query["dob"] = intval($searchDate);
                }
            }
        } else {
            $query['dob'] = array('$ne' => intval(0), '$exists' => true);
        }
        if ($params['country_id'] > 0) {
            $query['country'] = intval($params['country_id']);
            if ($params['city'])
                $query['city'] = new MongoRegex("/^" . $params['city'] . "$/i");
        }
        else {
            if ($params['longitude'] && $params['latitude']) {
                $long = $params['longitude'];
                $lat = $params['latitude'];
                //default 2 kms
                $distance = $params['distance'] ? $params['distance'] : 2;
                $query['location'] = array('$near' => array(floatval($long), floatval($lat)), '$maxDistance' => floatval($distance / 111.12));
            }
        }
        $query['matchmaker'] = 1;
        $query['visibility'] = array('$ne' => intval(0));

        //now we dont have to retrun any users who like/dislike me or I have liked/disliked one
        $myDetails = SELF::getMatchmakerDetails($userId);
        $skipUsers = array_merge($myDetails['like'], $myDetails['dislike']);
        $skipUsers[] = $userId;
        foreach ($skipUsers as $skip) {
            $arr[] = array('_id' => array('$ne' => "" . $skip . ""));
        }
        $query['$and'] = $arr;
        $query['dislike'] = array('$nin' => array($userId));
        $query['like'] = array('$nin' => array($userId));

        //keyword or hashtag search
        if ($params['keyword']) {
            $keyword = json_decode($params['keyword']);
            if (is_array($keyword))
                $query['keyword'] = array('$in' => $keyword);
            else
                $query['keyword'] = array('$in' => array($params['keyword']));
        }
        $result = $collection->find($query)
                ->skip($pageOffset)
                ->limit($pageCount);

        if ($result) {
            $data = array();
            foreach ($result as $user) {
                $data = SELF::formatUserData($user, $userId);
                $response[] = $data;
            }
        }

        /* users which do not have location with them */
        /* $result_bal = SELF::getNonLocationUsers($userId, $params);
          if($result_bal){
          $data = array();
          foreach ($result_bal as $user) {
          $data['user_id'] 	= $user['_id'];
          $data['nickname'] 	= $user['nickname'];
          $data['dob'] 		= $user['dob']?substr($user['dob'],0, 4).'-'.substr($user['dob'],4, 2).'-'.substr($user['dob'],6, 2):"";
          if($user['gender'] == 1)
          $gender 		= 'male';
          else if($user['gender'] == 2)
          $gender 		= 'female';
          else if($user['gender'] == 3)
          $gender 		= 'ts';
          else
          $gender 		= '';

          $data['gender'] 	= $gender;
          $data['city'] 		= $user['city'];
          $data['country'] 	= Countries::getCountryName(intval($user['country']));
          $data['location']	= $user['location']?$user['location']:array();
          $response[] = $data;
          $i++;
          if($i == $limit){
          return $response;
          }
          }
          } */
        return $response;
    }

    public static function getNonLocationUsers($userId, $params) {
        $country_code = Users::model()->findByPk($userId)->country_id;
        if ($country_code > 0) {
            $query['country'] = intval($country_code);
        }

        $query['$or'] = array(
            0 => array(
                'location' => array(intval(0), intval(0))
            ),
            1 => array(
                'location' => array(
                    '$exists' => false
                )
            ),
            2 => array(
                'location' => array(
                )
            )
        );

        if ($params['gender']) {
            if ($params['gender'] == 'male') {
                $query['gender'] = 1;
            } else if ($params['gender'] == 'female') {
                $query['gender'] = 2;
            } else if ($params['gender'] == 'ts') {
                $query['gender'] = 3;
            } else if ($params['gender'] == 'other') {
                $query['gender'] = array('$exists' => false);
            }
        } else {
            $query['gender'] = array('$ne' => intval(0), '$exists' => true);
        }
        if ($params['age']) {
            if (Common::isJson($params['age'])) {
                $age = json_decode($params['age'], true);
                $searchDate1 = date('Ymd', strtotime(date('Y-m-d') . ' - ' . intval($age[0]) . ' year'));
                $searchDate2 = date('Ymd', strtotime(date('Y-m-d') . ' - ' . intval($age[1]) . ' year'));
                $query["dob"] = array('$lte' => intval($searchDate1),
                    '$gte' => intval($searchDate2));
            } else {
                $searchDate = date('Ymd', strtotime(date('Y-m-d') . ' - ' . intval(substr($params['age'], 2)) . ' year'));

                if (substr($params['age'], 0, 2) == 'lt') {
                    $query["dob"] = array('$gt' => intval($searchDate));
                }

                if (substr($params['age'], 0, 2) == 'gt') {
                    $query["dob"] = array('$lt' => intval($searchDate));
                }

                if (substr($params['age'], 0, 2) == 'eq') {
                    $query["dob"] = intval($searchDate);
                }
            }
        } else {
            $query['dob'] = array('$ne' => intval(0), '$exists' => true);
        }

        //now we dont have to retrun any users who like/dislike me or I have liked/disliked one
        $myDetails = SELF::getMatchmakerDetails($userId);
        $skipUsers = array_merge($myDetails['like'], $myDetails['dislike']);
        $skipUsers[] = $userId;
        foreach ($skipUsers as $skip) {
            $arr[] = array('_id' => array('$ne' => "" . $skip . ""));
        }
        $query['$and'] = $arr;

        $query['dislike'] = array('$nin' => array(intval($userId)));
        $query['like'] = array('$nin' => array(intval($userId)));
        $collection = Yii::app()->edmsMongoCollection('matchmaker');
        $result = $collection->find($query);
        return $result;
    }

    public static function getMatches($userId, $like = 'like') {
        //both pressed like on each other

        $collection = Yii::app()->edmsMongoCollection('matchmaker');
        $data = $collection->findOne(array(
            "_id" => $userId
        ));
        $matches = array();
        $likes = $data['like'];
        if (sizeof($likes) > 0) {
            $query['_id'] = array('$in' => $likes);
            $query['like'] = array('$in' => array(strval($userId)));
            $query['gender'] = array('$ne' => intval(0), '$exists' => true);
            $query['dob'] = array('$ne' => intval(0), '$exists' => true);
            $query['visibility'] = array('$ne' => intval(0));
            $result = $collection->find($query);
            foreach ($result as $user) {
                $data = SELF::formatUserData($user, $userId);
                $matches[] = $data;
            }
        }
        return $matches;
    }

    public static function getMatchmakerStatus($userId) {
        $collection = Yii::app()->edmsMongoCollection('matchmaker');
        $data = $collection->findOne(array(
            "_id" => $userId,
            'matchmaker' => intval(1)
        ));

        if ($data) {
            return true;
        }

        return false;
    }

    public static function getMatchmakerUsers($userId, $params) {
        //users that have matchmaker on and fit the criteria in search male, female, age and distance
        if (!isset($params['offset']) || !is_numeric($params['offset']))
            $pageOffset = 0;
        else
            $pageOffset = $params['offset'];

        if (!isset($params['count']) || !is_numeric($params['count']))
            $pageCount = 100;
        else
            $pageCount = $params['count'];

        $collection = Yii::app()->edmsMongoCollection('matchmaker');
        $sort = array();
        if ($params['gender']) {
            if ($params['gender'] == 'male') {
                $query['gender'] = 1;
            } else if ($params['gender'] == 'female') {
                $query['gender'] = 2;
            } else if ($params['gender'] == 'ts') {
                $query['gender'] = 3;
            } else if ($params['gender'] == 'other') {
                $query['gender'] = array('$exists' => false);
            }
        } else {
            $query['gender'] = array('$ne' => intval(0), '$exists' => true);
        }
        $query['visibility'] = array('$ne' => intval(0));
        if ($params['age']) {
            if (Common::isJson($params['age'])) {
                $age = json_decode($params['age'], true);
                $searchDate1 = date('Ymd', strtotime(date('Y-m-d') . ' - ' . intval($age[0]) . ' year'));
                $searchDate2 = date('Ymd', strtotime(date('Y-m-d') . ' - ' . intval($age[1]) . ' year'));
                $query["dob"] = array('$lte' => intval($searchDate1),
                    '$gte' => intval($searchDate2));
            } else {
                $searchDate = date('Ymd', strtotime(date('Y-m-d') . ' - ' . intval(substr($params['age'], 2)) . ' year'));

                if (substr($params['age'], 0, 2) == 'lt') {
                    $query["dob"] = array('$gt' => intval($searchDate));
                }

                if (substr($params['age'], 0, 2) == 'gt') {
                    $query["dob"] = array('$lt' => intval($searchDate));
                }

                if (substr($params['age'], 0, 2) == 'eq') {
                    $query["dob"] = intval($searchDate);
                }
            }
        } else {
            $query['dob'] = array('$ne' => intval(0), '$exists' => true);
        }

        if ($params['longitude'] && $params['latitude']) {
            $long = $params['longitude'];
            $lat = $params['latitude'];
            //default 2 kms
            $distance = $filter['distance'] ? $filter['distance'] : 2;
            $query["location"] = array('$near' => array(floatval($long), floatval($lat)), '$maxDistance' => floatval($distance / 111.12));
        } else {
            //random select
            $array_key = array('_id', 'nickname', 'last_name', 'dob', 'gender', 'country');
            $array_sort = array(1, -1);
            $rand_sort = $array_sort[array_rand($array_sort)];
            $rand_sort1 = $array_sort[array_rand($array_sort)];
            $rand1 = $array_key[array_rand($array_key)];
            $rand = $array_key[array_rand($array_key)];
            if ($rand)
                $sort[$rand] = $rand_sort;
            if ($rand1)
                $sort[$rand1] = $rand_sort1;
        }
        $query["matchmaker"] = 1;

        //now we dont have to retrun any users who like/dislike me or I have liked/disliked one
        $myDetails = SELF::getMatchmakerDetails($userId);
        $skipUsers = array_merge($myDetails['like'], $myDetails['dislike']);
        $skipUsers[] = $userId;
        foreach ($skipUsers as $skip) {
            $arr[] = array('_id' => array('$ne' => "" . $skip . ""));
        }
        $query['$and'] = $arr;
        $query['dislike'] = array('$nin' => array($userId));
        $query['like'] = array('$nin' => array($userId));
        $result = $collection->find($query)
                ->sort($sort)
                ->skip($pageOffset)
                ->limit($pageCount);
        if ($result) {
            $data = array();
            $i = 0;
            foreach ($result as $user) {
                $data = SELF::formatUserData($user, $userId);
                $response[] = $data;
            }
        }
        return $response;
    }

    public static function getPendingLikes($userId, $params) {
        if (!isset($params['offset']))
            $pageOffset = 0;
        else
            $pageOffset = $params['offset'];
        if (!is_numeric($pageOffset)) {
            $pageOffset = 0;
        }

        if (!isset($params['count']))
            $pageCount = 0;
        else
            $pageCount = $params['count'];

        if (!is_numeric($params['count'])) {
            $pageCount = 100;
        }

        $collection = Yii::app()->edmsMongoCollection('matchmaker');

        //get users likes
        $data = $collection->findOne(array(
            "_id" => $userId
        ));
        $likes = $data['like'];
        $likes[] = $userId;
        $dislike = $data['dislike'];
        $dislike[] = $userId;
        $query['_id'] = array('$nin' => $likes);
        $query['_id'] = array('$nin' => $dislike);
        $query['like'] = array('$in' => array(strval($userId)));
        $query['gender'] = array('$ne' => intval(0), '$exists' => true);
        $query['dob'] = array('$ne' => intval(0), '$exists' => true);

        $result = $collection->find($query)
                ->skip($pageOffset)
                ->limit($pageCount);
        if ($result) {
            $data = array();
            foreach ($result as $user) {
                $data = SELF::formatUserData($user, $userId);
                $response[] = $data;
            }
        }
        return $response;
    }

    public static function updateUserStatus($params, $userId) {
        if ($userId) {
            $update = new stdClass();
            if ($params['status']) {
                $update->status = $params['status'];
            }
            if (isset($params['visibility'])) {
                if ($params['visibility'] == 'visible')
                    $update->visibility = intval(1);
                else
                    $update->visibility = intval(0);
            }
            if (isset($params['matchmaker'])) {
                $update->matchmaker = $params['matchmaker'];
            }
            if (Common::isValidLatitude($params['latitude']) && Common::isValidLongitude($params['longitude'])) {
                $update->location = array(floatval($params['longitude']), floatval($params['latitude']));
            }
            $collection = Yii::app()->edmsMongoCollection('matchmaker');
            $collection->update(array(
                "_id" => $userId
                    ), array('$set' => $update));
        }
    }

}
