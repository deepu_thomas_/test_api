<tr style="background-color: #eb3f3c;">
    <td>
        <h1 style="color:#FFF; margin-bottom:0px; margin-top:50px;"><i><b style='text-transform:uppercase'>Reset Pin Code</b></i></h1>
    </td>
</tr>
<tr>
    <td>		
        <p>
            Your tag <?php echo $tagName; ?> has been attacked by <?php echo $user_email; ?>.
            <br/>
            <?php echo $warning; ?>            
        </p>
        <p>
            <?php echo Yii::t("labels", "Thank you!"); ?><br><br>
            <b><?php echo Yii::t("labels", "Tagwild"); ?></b>
        </p>
    </td>
</tr>

